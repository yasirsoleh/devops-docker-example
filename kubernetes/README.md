# Kubernetes

## How to run

```
kubectl apply -f namespace.yml
kubectl apply -f .
```

## How to access

One time setup

### 1. Enable ingress in minikube

```
minikube addons enable ingress
```

### 2. Add devops-docker.example and minikube ip in /etc/hosts

Get minikube ip

```
minikube ip
```

Then add that ip to hosts

```
192.168.100.1 devops-docker.example
```

Try accessing using curl

```
curl devops-docker.example
```
