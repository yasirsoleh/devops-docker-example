# Devops Docker Example

## Docker Compose

#### Prepare .env

```
cp .env.example .env
```

#### 1. Build

```
docker compose build
```

#### 2. Start

```
docker compose up
```

#### 3. Stop

```
docker compose down
```

## Individual Container

### Database

#### 1. Run the container

```
docker run --name database-container-name -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=database_name MYSQL_USER=username MYSQL_PASSWORD=passowrd mysql
```

### Backend

#### 1. Open backend folder

#### 2. Build the image

```
docker build -t backend-image-name .
```

#### 3. Run the container

```
docker run --name backend-container-name -p 8000:8000 -e MYSQL_HOST=database-container-name -e MYSQL_PORT=3306 -e MYSQL_DATABASE=database_name MYSQL_USER=username MYSQL_PASSWORD=passowrd backend-image-name
```

### Frontend

#### 1. Open frontend folder

#### 2. Build the image

```
docker build -t frontend-image-name .
```

#### 3. Run the container

```
docker run --name frontend-container-name -p 5173:5173 frontend-image-name
```
